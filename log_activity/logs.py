from log_activity.models import LogActivity, ADDITION, CHANGE, DELETION
from django.contrib.admin.options import get_content_type_for_model


def log_addition(request, obj, message):
    """
    Log that an object has been successfully added.

    The default implementation creates an admin LogEntry object.
    """

    return LogActivity.objects.log_action(
        user_id=request.user.pk,
        content_type_id=get_content_type_for_model(obj).pk,
        object_id=obj.pk,
        object_repr=str(obj),
        action_flag=ADDITION,
        change_message=message,
        request=request,
    )

def log_change(request, obj, message):
    """
    Log that an object has been successfully changed.

    The default implementation creates an admin LogEntry object.
    """

    return LogActivity.objects.log_action(
        user_id=request.user.pk,
        content_type_id=get_content_type_for_model(obj).pk,
        object_id=obj.pk,
        object_repr=str(obj),
        action_flag=CHANGE,
        change_message=message,
        request=request,
    )

def log_deletion(request, obj, object_repr):
    """
    Log that an object will be deleted. Note that this method must be
    called before the deletion.

    The default implementation creates an admin LogEntry object.
    """

    return LogActivity.objects.log_action(
        user_id=request.user.pk,
        content_type_id=get_content_type_for_model(obj).pk,
        object_id=obj.pk,
        object_repr=object_repr,
        action_flag=DELETION,
        request=request,
    )
