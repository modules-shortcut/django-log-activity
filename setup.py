from setuptools import setup, find_packages

setup(
    name='log_activity',
    version='1.0',
    author='dayat@rumahlogic.com',
    description='log activity',
    packages=find_packages(),
)